<?php

/**
 * Test the webform analysis export functionality.
 */
class WebformAnalysisExportTest extends WebformAnalysisExportTestBase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => t('Webform Analysis Export Test'),
      'description' => t('Test the analysis export.'),
      'group' => t('Webform Analysis Export'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    return parent::setUp(array(
      'webform',
      'webform_analysis_export',
    ));
  }

  /**
   * Test the analysis export.
   */
  public function testWebformAnalysisExport() {
    $node = $this->createWebformNode(array(), array(
      array(
        'name' => 'Test Select',
        'type' => 'select',
        'weight' => 1,
        'form_key' => 'test_select',
        'extra' => array(
          'items' => "a|A\nb|B\nc|C",
        ),
      ),
    ));

    // Submit "A" twice and "B" once.
    $this->drupalPost('node/' . $node->nid, array(
      'submitted[test_select]' => 'a',
    ), 'Submit');
    $this->drupalPost('node/' . $node->nid, array(
      'submitted[test_select]' => 'a',
    ), 'Submit');
    $this->drupalPost('node/' . $node->nid, array(
      'submitted[test_select]' => 'b',
    ), 'Submit');

    // Login as an admin and export the analysis.
    $this->drupalLogin($this->createWebformUser());
    $this->drupalPost('node/' . $node->nid . '/webform-results/analysis', array(
      'export_format' => 'AnalysisExportFormatCSV',
    ), 'Export');

    $this->assertText('"Webform analysis of ""Test Webform"""');
    $this->assertText('"Test Select"');
    $this->assertText('A,2');
    $this->assertText('B,1');
  }

}
