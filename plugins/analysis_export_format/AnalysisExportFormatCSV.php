<?php

$plugin = array(
  'title' => t('CSV'),
  'description' => t('Export analysis results as a CSV.'),
  'weight' => 0,
  'class' => array(
    'class' => 'AnalysisExportFormatCSV',
    'file' => 'AnalysisExportFormatCSV.php',
  ),
);

/**
 * A CSV analysis export format.
 */
class AnalysisExportFormatCSV extends AnalysisExportFormatBase {

  /**
   * {@inheritdoc}
   */
  public function getContent($analysis_data, $file, $node) {
    // Describe the contents of the file and create an empty line.
    fputcsv($file, array(t('Webform analysis of "!node"', array('!node' => $node->title))));
    fputcsv($file, array());

    foreach ($analysis_data as $analysis_datum) {
      // Write the name of the component.
      fputcsv($file, array($analysis_datum['component']['name']));

      // If the analysis has a table header, write that first.
      if (!empty($analysis_datum['table_header'])) {
        fputcsv($file, $analysis_datum['table_header']);
      }

      // Write the "table_rows" to the csv.
      foreach ($analysis_datum['table_rows'] as $row) {
        fputcsv($file, $row);
      }

      // If this analysis includes "other_data", add those too.
      if (!empty($analysis_datum['other_data'])) {
        foreach ($analysis_datum['other_data'] as $row) {
          fputcsv($file, $row);
        }
      }

      // Write an empty line to separate components.
      fputcsv($file, array());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getContentType() {
    return 'application/csv';
  }

  /**
   * {@inheritdoc}
   */
  public function getFileExtension() {
    return 'csv';
  }

}
