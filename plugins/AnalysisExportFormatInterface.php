<?php

/**
 * An interface for analysis export formats.
 */
interface AnalysisExportFormatInterface {

  /**
   * Get the value of the Content-Type header to send when delivering the file.
   *
   * @return string
   *   The file Content-Type.
   */
  public function getContentType();

  /**
   * Get the content in the given export format.
   *
   * @param array $analysis_data
   *   Webform analysis data.
   * @param mixed $file
   *   The file resource to add content to.
   * @param stdClass $node
   *   The node being analysed.
   */
  public function getContent($analysis_data, $file, $node);

  /**
   * Get the file extension to use when downloading the file.
   */
  public function getFileExtension();

}
