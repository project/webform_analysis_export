<?php

/**
 * The analysis format plugin manager.
 */
class AnalysisExportFormatManager {

  /**
   * Get the analysis export format plugins.
   *
   * @return array
   *   An array of plugin definitions.
   */
  public static function getDefinitions() {
    ctools_include('plugins');
    $plugins = ctools_get_plugins('webform_analysis_export', 'analysis_export_format');
    uasort($plugins, 'ctools_plugin_sort');
    return $plugins;
  }

  /**
   * Creates an instance of the analysis plugin.
   *
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return \AnalysisExportFormatInterface
   */
  public static function createInstance($plugin_id) {
    $definitions = static::getDefinitions();
    if (!isset($definitions[$plugin_id])) {
      throw new \InvalidArgumentException(sprintf('Plugin %s does not exist', $plugin_id));
    }
    $class = ctools_plugin_get_class($definitions[$plugin_id], 'class');
    return new $class;
  }

}
